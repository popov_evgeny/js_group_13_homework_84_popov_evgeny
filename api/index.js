const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const app = express();
const users = require('./routes/user.route')
const tasks = require('./routes/task.route')

const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use('/users', users);
app.use('/tasks', tasks);

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  app.listen(port, () => {
    console.log(`Todo server ${port}`)
  });

  process.on('exit', () => {
    mongoose.disconnect();
  });
}

run().catch(e => console.error(e));