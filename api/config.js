const rootPath = __dirname;

module.exports = {
  rootPath,
  mongo: {
    db: 'mongodb://localhost/todo',
    options: {useNewUrlParser: true},
  }
}