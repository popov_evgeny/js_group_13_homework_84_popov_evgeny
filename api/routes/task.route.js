const express = require('express');
const Tasks = require('../models/Task');


const router = express.Router();

router.get('/', async (req,res) => {
  const tasks = await Tasks.find().populate('user', 'username');
  return res.send(tasks);
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.title) {
      return res.status(500).send({message: 'Enter task please!'});
    }

    const taskData = {
      user: null,
      title: req.body.title,
      status: 'new'
    }

    if (req.body.user !== '') {
      taskData.user = req.body.user;
    }

    const task = new Tasks(taskData);
    await task.save();
    return res.send(task);

  } catch (e) {
    next(e);
  }
});

router.put('/:id', async (req, res, next) => {
  try {
    if (req.body.user) {
      if (req.body.user === 'null'){
        await Tasks.updateOne({_id: req.params.id}, {user: null});
      } else {
        await Tasks.updateOne({_id: req.params.id}, {user: req.body.user});
      }
    }

    if (req.body.status) {
      await Tasks.updateOne({_id: req.params.id}, {status: req.body.status});
    }

    return res.send({message: 'The task was changed!'});

  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req,res) => {
  await Tasks.deleteOne({_id: req.params.id});
  return res.send({message: 'Delete task id=' + req.params.id})
});

module.exports = router;