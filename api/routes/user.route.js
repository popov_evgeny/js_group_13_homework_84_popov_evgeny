const express = require('express');
const Users = require('../models/User');


const router = express.Router();

router.get('/', async (req,res, next) => {
  try {
    const users = await Users.find();
    return res.send(users);
  } catch (e) {
    next(e)
  }
});

router.post('/', async (req,res, next) => {
  try {
    if (!req.body.username){
      return res.status(500).send({message: 'Enter user name please!'});
    }

    const userData = {
      username: req.body.username
    }

    const user = new Users(userData);
    await user.save();
    return res.send(userData);

  } catch (e) {
    next(e);
  }

});

module.exports = router;