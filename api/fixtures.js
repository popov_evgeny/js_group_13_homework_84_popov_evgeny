const mongoose = require('mongoose');
const config = require("./config");
const Task = require("./models/Task");
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin, guest] = await User.create({
    username: 'user',
  }, {
    username: 'admin',
  }, {
    username: 'guest',
  });

  await Task.create({
    user: user,
    title: 'New task user',
    status: 'new'
  }, {
    user: admin,
    title: 'New task admin',
    status: 'new'
  }, {
    user: guest,
    title: 'New task guest',
    status: 'new'
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));