import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { createTasksRequest } from '../../store/task.actions';
import { AddTask } from '../../models/task.model';
import { Observable } from 'rxjs';
import { UserModel } from '../../models/user.model';
import { fetchUsersRequest } from '../../store/user.actions';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {
  @ViewChild('form') form!: NgForm;
  users: Observable<UserModel[]>;
  loading: Observable<boolean>;
  error: Observable<string | null>;


  constructor(private store: Store<AppState>) {
    this.users = store.select( state => state.users.users);
    this.loading = store.select( state => state.tasks.createLoading);
    this.error = store.select( state => state.tasks.createError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchUsersRequest());
  }

  onSubmit() {
    const newTask: AddTask = {
      title: this.form.value.title,
      user: this.form.value.user
    };
    this.store.dispatch(createTasksRequest({newTask}));
  }

}
