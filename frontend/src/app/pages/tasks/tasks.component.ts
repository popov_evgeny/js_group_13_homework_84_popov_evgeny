import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserModel } from '../../models/user.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchUsersRequest } from '../../store/user.actions';
import { EditTaskStatus, EditTaskUser, TaskModel } from '../../models/task.model';
import { editTasksRequest, fetchTasksRequest, removeTasksRequest } from '../../store/task.actions';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.sass']
})
export class TasksComponent implements OnInit {
  users: Observable<UserModel[]>;
  loading: Observable<boolean>;
  loadingRemove: Observable<boolean>;
  tasks: Observable<TaskModel[]>;
  isRemoveId = '';

  constructor(private store: Store<AppState>) {
    this.users = store.select( state => state.users.users);
    this.loading = store.select( state => state.users.fetchLoading);
    this.tasks = store.select( state => state.tasks.tasks);
    this.loading = store.select( state => state.tasks.fetchLoading);
    this.loadingRemove = store.select( state => state.tasks.removeLoading);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchUsersRequest());
    this.store.dispatch(fetchTasksRequest());
  }

  editTaskName(id: string,event: Event) {
    const value: EditTaskUser = {
      _id: id,
      user: (<HTMLSelectElement>event.target).value
    };
    this.store.dispatch(editTasksRequest({editTask: value}));
  }

  editTaskStatus(id: string, event: Event) {
    const value: EditTaskStatus = {
      _id: id,
      status: (<HTMLSelectElement>event.target).value,
    };
    this.store.dispatch(editTasksRequest({editTask: value}));
  }

  removeTask(id: string) {
    setTimeout(() => {
      this.store.dispatch(removeTasksRequest({idTask: id}));
    }, 3000)
    this.isRemoveId = id;
  }
}
