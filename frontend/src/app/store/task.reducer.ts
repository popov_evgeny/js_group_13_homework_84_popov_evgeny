import { TaskState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createTasksRequest,
  createTasksSuccess,
  createTasksFailure,
  fetchTasksFailure,
  fetchTasksRequest,
  fetchTasksSuccess, editTasksRequest, editTasksSuccess, editTasksFailure, removeTasksRequest, removeTasksSuccess
} from './task.actions';

const initialState: TaskState = {
  tasks: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  editLoading: false,
  editError: null,
  removeLoading: false,
  removeError: null
}

export const taskReducer = createReducer(
  initialState,
  on(fetchTasksRequest, state => ({...state, fetchLoading: true})),
  on(fetchTasksSuccess, (state, {tasks}) => ({...state, fetchLoading: false, tasks})),
  on(fetchTasksFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createTasksRequest, state => ({...state, createLoading: true})),
  on(createTasksSuccess, state => ({...state, createLoading: false})),
  on(createTasksFailure, (state, {error}) => ({...state, createLoading: false, createError: error,})),

  on(editTasksRequest, state => ({...state, editLoading: true})),
  on(editTasksSuccess, state => ({...state, editLoading: false})),
  on(editTasksFailure, (state, {error}) => ({...state, editLoading: false, createError: error,})),

  on(removeTasksRequest, (state, {idTask}) => {
    const isDelete = state.tasks.filter(task => {
      return task._id !== idTask;
    })
    return {...state, tasks: isDelete , removeLoading: true}
  }),
  on(removeTasksSuccess, state => ({...state, removeLoading: false})),
  on(createTasksFailure, (state, {error}) => ({...state, removeLoading: false, removeError: error,}))
);
