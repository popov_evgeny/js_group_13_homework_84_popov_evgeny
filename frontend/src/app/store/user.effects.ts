import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TodoService } from '../service/todo.service';
import { fetchUsersFailure, fetchUsersRequest, fetchUsersSuccess } from './user.actions';
import { mergeMap, map, catchError, of } from 'rxjs';


@Injectable()

export class UserEffects {
  fetchUsers = createEffect(() => this.actions.pipe(
    ofType(fetchUsersRequest),
    mergeMap(() => this.todoService.getUsers().pipe(
      map( users => fetchUsersSuccess({users})),
      catchError( () => of(fetchUsersFailure({error: 'Error!'})))
    )
  )));

  constructor(
    private actions: Actions,
    private todoService: TodoService
  ) {}
}
