import { createAction, props } from '@ngrx/store';
import { AddTask, EditTaskStatus, EditTaskUser, TaskModel } from '../models/task.model';

export const fetchTasksRequest = createAction('[Tasks] Fetch Request');
export const fetchTasksSuccess = createAction('[Tasks] Fetch Success', props<{tasks: TaskModel[]}>());
export const fetchTasksFailure = createAction('[Tasks] Fetch Failure', props<{error: string}>());

export const createTasksRequest = createAction('[Tasks] Create Request', props<{newTask: AddTask}>());
export const createTasksSuccess = createAction('[Tasks] Create Success');
export const createTasksFailure = createAction('[Tasks] Create Failure', props<{error: string}>());


export const editTasksRequest = createAction('[Tasks] Edit Request', props<{editTask: EditTaskUser | EditTaskStatus}>());
export const editTasksSuccess = createAction('[Tasks] Edit Success');
export const editTasksFailure = createAction('[Tasks] Edit Failure', props<{error: string}>());

export const removeTasksRequest = createAction('[Tasks] Remove Request', props<{idTask: string}>());
export const removeTasksSuccess = createAction('[Tasks] Remove Success');
export const removeTasksFailure = createAction('[Tasks] Remove Failure', props<{error: string}>());
