import { UserModel } from '../models/user.model';
import { TaskModel } from '../models/task.model';

export type UserState = {
  users: UserModel[],
  fetchLoading: boolean,
  fetchError: null | string
}

export type TaskState = {
  tasks: TaskModel[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  editLoading: boolean,
  editError: null | string,
  removeLoading: boolean,
  removeError: null | string
}

export type AppState = {
  users: UserState,
  tasks: TaskState
}
