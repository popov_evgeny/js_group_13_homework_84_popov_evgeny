import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  createTasksFailure,
  createTasksRequest,
  createTasksSuccess,
  editTasksFailure,
  editTasksRequest,
  editTasksSuccess,
  fetchTasksFailure,
  fetchTasksRequest,
  fetchTasksSuccess,
  removeTasksFailure,
  removeTasksRequest,
  removeTasksSuccess
} from './task.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { TodoService } from '../service/todo.service';
import { Router } from '@angular/router';

@Injectable()

export class TaskEffects {
  fetchTasks = createEffect(() => this.actions.pipe(
    ofType(fetchTasksRequest),
    mergeMap(() => this.todoService.getTasks().pipe(
      map( tasks => fetchTasksSuccess({tasks})),
      catchError( () => of(fetchTasksFailure({error: 'Error!'})))
    )
  )));

  createTasks = createEffect(() => this.actions.pipe(
    ofType(createTasksRequest),
    mergeMap(({newTask}) => this.todoService.addTask(newTask).pipe(
      map(() => createTasksSuccess()),
      tap( () => this.router.navigate(['/'])),
      catchError(() => of(createTasksFailure({error: 'Error!'})))
    ))
  ));

  editTasks = createEffect(() => this.actions.pipe(
    ofType(editTasksRequest),
    mergeMap(({editTask}) => this.todoService.editTask(editTask).pipe(
      map(() => editTasksSuccess()),
      catchError(() => of(editTasksFailure({error: 'Error!'}))))
    )
  ));

  removeTasks = createEffect(() => this.actions.pipe(
    ofType(removeTasksRequest),
    mergeMap(({idTask}) => this.todoService.removeTask(idTask).pipe(
      map(() => removeTasksSuccess()),
      catchError(() => of(removeTasksFailure({error: 'Error!'}))))
    )
  ));

  constructor(
    private actions: Actions,
    private todoService: TodoService,
    private router: Router
  ) {}
}
