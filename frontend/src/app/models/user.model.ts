export class UserModel {
  constructor(
    public _id: string,
    public username: string
  ) {}
}

export interface UserInterface {
  _id: string,
  username: string
}
