export class TaskModel {
  constructor(
    public _id: string,
    public user: {
      _id: string,
      username: string
    },
    public title: string,
    public status: string
  ) {
  }
}

export interface TaskInterface {
  _id: string,
  user: {
   _id: string,
   username: string
  },
  title: string,
  status: string
}

export interface AddTask {
  title: string,
  user: string | null
}


export interface EditTaskUser {
  _id: string,
  user: string | null
}

export interface EditTaskStatus {
  _id: string,
  status: string
}
