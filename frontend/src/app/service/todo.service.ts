import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserInterface, UserModel } from '../models/user.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { AddTask, EditTaskStatus, EditTaskUser, TaskInterface, TaskModel } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get<UserInterface[]>(environment.apiUrl + '/users').pipe( map( response => {
      return response.map( usersData => {
        return new UserModel(
          usersData._id,
          usersData.username
        )});
    }));
  }


  getTasks() {
    return this.http.get<TaskInterface[]>(environment.apiUrl + '/tasks').pipe( map( response => {
      return response.map( taskData => {
        return new TaskModel(
          taskData._id,
          taskData.user,
          taskData.title,
          taskData.status
        )});
    }));
  }

  addTask(task: AddTask) {
    return this.http.post(environment.apiUrl + '/tasks', task);
  }

  editTask(editTask: EditTaskUser | EditTaskStatus) {
    return this.http.put(environment.apiUrl + '/tasks/' + editTask._id, editTask);
  }

  removeTask(id: string) {
    return this.http.delete(environment.apiUrl + '/tasks/' + id);
  }
}
